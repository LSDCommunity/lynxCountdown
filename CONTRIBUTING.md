### Contribuir

- [Reportar Errores](https://gitlab.com/LSDCommunity/lynxCountdown/-/issues)
- Tweet about it :)
- Invitanos un cafe

#### Pull Requests
- **Resolver un problema**
- Para mejorar el código, use [ESLint](https://eslint.org/) como herramienta de calidad de código.
- Pequeña es mejor que grande.
