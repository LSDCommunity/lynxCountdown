# Lynx CountDown

## ¿Por qué otra cuenta atrás?

Tras el uso de simplyCountdown.js y al detectar el error de UTC que complicaba mucho la universalización de la cuenta atrás y al ver que su desarrollador no aceptaba pull request decidí crear un fork con las correcciones realizadas.

Este es un Javascript CountDown muy (muy) básico.

## Empezando

Lamentablemente el proyecto no se encuentra en npm de momento pero si puedes importarlo desde la web o descargarlo para su uso.

```html
<link
  rel="stylesheet"
  href="https://jugamos.patojad.com.ar/css/estilos.css"/>
<script
  src="https://jugamos.patojad.com.ar/js/lynxCountdown.js">
</script>
```


### Configurarlo


```javascript
    // This is an example with default parameters

    lynxCountdown('[CSS-SELECTOR]', {
            year: 2019, // required
            month: 6, // required
            day: 28, // required
            hours: 0, // Default is 0 [0-23] integer
            minutes: 0, // Default is 0 [0-59] integer
            seconds: 0, // Default is 0 [0-59] integer
            words: { //words displayed into the countdown
                days: 'day',
                hours: 'hour',
                minutes: 'minute',
                seconds: 'second',
                pluralLetter: 's'
            },
            plural: true, //use plurals
            inline: false, //set to true to get an inline basic countdown like : 24 days, 4 hours, 2 minutes, 5 seconds
            inlineClass: 'lynx-countdown-inline', //inline css span class in case of inline = true
            // in case of inline set to false
            enableUtc: true, //Use UTC as default
            onEnd: function() { return; } //Callback on countdown end, put your own function here
            refresh: 1000, // default refresh every 1s
            sectionClass: 'lynx-section', //section css class
            amountClass: 'lynx-amount', // amount css class
            wordClass: 'lynx-word', // word css class
            zeroPad: false,
            countUp: false
    });
```

## Fácil de personalizar

Puede personalizar fácilmente la cuenta regresiva usando el archivo de inicio del tema CSS o crear el suyo propio así:

/! \ La siguiente plantilla de tema funciona con la clase predeterminada en los parámetros.

 ```css
    /*
    * Project : lynx-countdown
    * File : lynxCountdown.theme.custom
    * Author : Your Name <your-mail[at]example.com>
    */

    .lynx-countdown {
        /* The countdown */
    }
    .lynx-countdown > .lynx-section {
        /* coutndown blocks */
    }

    .lynx-countdown > .lynx-section > div {
        /* countdown block inner div */
    }

    .lynx-countdown > .lynx-section .lynx-amount,
    .lynx-countdown > .lynx-section .lynx-word {
        /* amounts and words */
    }

    .lynx-countdown > .lynx-section .lynx-amount {
        /* amounts */
    }

    .lynx-countdown > .lynx-section .lynx-word {
        /* words */
   }
```

### Contributing

- [Reportar Errores](https://gitlab.com/LSDCommunity/lynxCountdown/-/issues)
- Tweet about it :)
- Invitanos un cafe


### Changelog

##### 1.5.1
- Resolve error in UTC for location
